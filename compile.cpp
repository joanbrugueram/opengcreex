/*
OpenGCReEx - Open source GameCube ISO extractor / compiler.
Copyright (C) 2015-2016 Joan Bruguera Micó <joanbrugueram@gmail.com>

This file is part of OpenGCReEx.

OpenGCReEx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenGCReEx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenGCReEx.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
#include <fstream>
#include <algorithm>
#include "compile.hpp"
#include "gcconsts.hpp"
#include "numericchecks.hpp"
#include "fst.hpp"

/**
 * Gets the alignment boundary of value, that is, the highest power of two
 * that evenly divides the given number.
 * Based on http://stackoverflow.com/a/2380818
 *
 * Params:
 *     v: The value from which the alignment boundary should be computed.
 * Returns: The alignment boundary of the given value. This will be a power of 2.
 */
template<typename T>
T getAlignmentBoundary(T v)
{
	if (v == 0)
	{
		throw std::runtime_error(StrFormat() << "Zero offset found in input. "
			<< "Your boot.bin or fst.bin is invalid.");
	}

	// ~v will invert all bit values (note that, then, v & ~v == 0).
	// Thus, all trailing zero bits will be converted to trailing ones.
	// By doing ~v + 1, we knock all those trailing ones to tralining zeros,
	// and we change the bit to the left of those trailing ones to a 1.
	// Thus, when we AND it again with v, we will only get this rightmost 1 bit,
	// thus, the resulting value will be the alignment boundary, as we wanted.
	return v & (~v + 1);
}

void compileGcIso(const filesystem::path &fsPath,
	const filesystem::path &isoPath, bool isDisc2)
{
	filesystem::path fsSysDirectory = fsPath / gcFsSysDirectoryName;
	filesystem::path fsRootDirectory = fsPath / gcFsRootDirectoryName;

	std::ofstream isoFile = openOutputBinaryFileWithExceptions(isoPath);

	// Read disc header
	std::vector<std::uint8_t> gcIsoHeader = readFileToMemory(
		fsSysDirectory / getGcIsoHeaderFileName(isDisc2));
	if (gcIsoHeader.size() != gcIsoHeaderSize)
	{
		throw std::runtime_error(StrFormat() << "GameCube ISO header (" <<
			getGcIsoHeaderFileName(isDisc2) << ") is not of the expected size.");
	}

	// Read bi2
	std::ifstream bi2Stream = openInputBinaryFileWithExceptions(
		fsSysDirectory / gcIsoBi2FileName);
	filesize_t bi2FileSize = getStreamSize(bi2Stream);
	if (bi2FileSize != gcIsoBi2Size)
	{
		throw std::runtime_error(StrFormat() << "GameCube disc header inforation BI2 (" <<
			gcIsoBi2FileName << ") is not of the expected size.");
	}

	// Read apploader
	std::ifstream appLoaderStream = openInputBinaryFileWithExceptions(
		fsSysDirectory / gcIsoAppLoaderFileName);
	filesize_t apploaderFileSize = getStreamSize(appLoaderStream);

	// Read DOL
	std::ifstream dolStream = openInputBinaryFileWithExceptions(fsSysDirectory / gcIsoDolFileName);
	filesize_t dolFileSize = getStreamSize(dolStream);

	// Read fst.bin
	std::vector<std::uint8_t> fstBinData = readFileToMemory(
		fsSysDirectory / getGcIsoFstFileName(isDisc2));
	std::unique_ptr<FstEntry> rootDirectory = readFstFile(fstBinData);

	// Before calculating the file offsets, regenerate the fst.bin file in order to get its size
	// This is a safeguard because there may be some fst.bin file which is technically valid but
	// uses more space than required. If this happens, then when we call writeFstFile later the
	// size of the fst.bin may change, which would break our file offset calculations.
	// By regenerating the fst.bin here, we are making sure the fst.bin size doesn't change later
	fstBinData = writeFstFile(*rootDirectory);

	// We need to read and calculate the size of the FST file of the other disc, if any, in order
	// to fill the "maximum FST size" field in the GameCube header
	std::vector<std::uint8_t> otherFstBinData;

	if (filesystem::is_regular_file(fsSysDirectory / getGcIsoFstFileName(!isDisc2)))
	{
		// Read the other fst.bin file
		otherFstBinData = readFileToMemory(fsSysDirectory / getGcIsoFstFileName(!isDisc2));

		// Rebuild the file to get the size accurately
		std::unique_ptr<FstEntry> otherRootDirectory = readFstFile(otherFstBinData);
		otherFstBinData = writeFstFile(*otherRootDirectory);
	}
	else
	{
		std::uint32_t oldFstFileSize = extractBigEndian<std::uint32_t>(gcIsoHeader.data(),
			gcIsoHeaderFstSizePosition);
		std::uint32_t oldMaxFstFileSize = extractBigEndian<std::uint32_t>(gcIsoHeader.data(),
			gcIsoHeaderFstMaxSizePosition);

		if (isDisc2 || oldFstFileSize != oldMaxFstFileSize)
		{
			std::cout << "WARNING: It appears that this is part of a multi disc image.\n" <<
				"However, the corresponding fst.bin file of the other disc was NOT found.\n" <<
				"This can result in a non-working result. Check the manual for more information." << std::endl;
		}
	}

	// Recompute offsets of the various elements in the filesystem
	std::cout << "Apploader size:" << apploaderFileSize << std::endl;

	std::uint32_t originalDolPosition = extractBigEndian<std::uint32_t>(
		gcIsoHeader.data(), gcIsoHeaderDolPositionPosition);
	fileoffset_t newDolPosition = checkedAdd<fileoffset_t>(gcIsoAppLoaderPosition, apploaderFileSize);
	newDolPosition = checkedAlign(newDolPosition, static_cast<fileoffset_t>(getAlignmentBoundary(originalDolPosition)));

	std::cout << "DOL offset :0x" << toHexString(newDolPosition, 8) << std::endl;
	std::cout << "DOL size   :0x" << toHexString(dolFileSize, 8) << std::endl;

	std::uint32_t originalFstBinPosition = extractBigEndian<std::uint32_t>(
		gcIsoHeader.data(), gcIsoHeaderFstPositionPosition);
	fileoffset_t newFstBinPosition = checkedAdd<fileoffset_t>(newDolPosition, dolFileSize);
	newFstBinPosition = checkedAlign(newFstBinPosition, static_cast<fileoffset_t>(getAlignmentBoundary(originalFstBinPosition)));

	std::cout << "FST offset :0x" << toHexString(newFstBinPosition, 8) << std::endl;
	std::cout << "FST size   :0x" << toHexString(fstBinData.size(), 8) << std::endl;
	std::cout << "FST entries:" << rootDirectory->recursiveCount() << std::endl;
	std::cout << "FST names  :0x" << toHexString(
		newFstBinPosition + rootDirectory->recursiveCount() * gcIsoFstEntrySize, 8) << std::endl;
	std::cout << std::endl;

	fileoffset_t fileSystemStartPosition = checkedAdd<fileoffset_t>(newFstBinPosition, fstBinData.size());
	fileoffset_t fileSystemCurrentPosition = fileSystemStartPosition;

	for (FstEntry &fstEntry : rootDirectory->recursiveGetEntries())
	{
		if (!fstEntry.isDirectory())
		{
			FstFile &fstFile = static_cast<FstFile &>(fstEntry);

			// Compute size of file to write
			std::ifstream fstFileStream = openInputBinaryFileWithExceptions(fsRootDirectory / fstFile.getFullPath());
			filesize_t fstFileStreamSize = getStreamSize(fstFileStream);

			// Align file system pointer on the same boundary as the original file
			fileSystemCurrentPosition = checkedAlign(fileSystemCurrentPosition,
				static_cast<fileoffset_t>(getAlignmentBoundary(fstFile.getPosition())));

			// Set new values on fst.bin file
			fstFile.setPosition(checkedCast<std::uint32_t>(fileSystemCurrentPosition));
			fstFile.setSize(checkedCast<std::uint32_t>(fstFileStreamSize));

			// Update file system pointer to the end of the file
			fileSystemCurrentPosition = checkedAdd<fileoffset_t>(fileSystemCurrentPosition, fstFileStreamSize);
		}
	}

	fstBinData = writeFstFile(*rootDirectory);

	// Patch DOL and fst.bin offsets in the disc header
	writeBigEndian(gcIsoHeader.data(), gcIsoHeaderDolPositionPosition,
		checkedCast<std::uint32_t>(newDolPosition));
	writeBigEndian(gcIsoHeader.data(), gcIsoHeaderFstPositionPosition,
		checkedCast<std::uint32_t>(newFstBinPosition));
	writeBigEndian(gcIsoHeader.data(), gcIsoHeaderFstSizePosition,
		checkedCast<std::uint32_t>(fstBinData.size()));
	writeBigEndian(gcIsoHeader.data(), gcIsoHeaderFstMaxSizePosition,
		checkedCast<std::uint32_t>(std::max(fstBinData.size(), otherFstBinData.size())));

	// Construct output ISO stream
	std::cout << "Compiling disc header..." << std::endl;
	writeToOutputStream(isoFile, gcIsoHeader.data(), gcIsoHeader.size());

	std::cout << "Compiling bi2..." << std::endl;
	copyStream(bi2Stream, isoFile, bi2FileSize);

	std::cout << "Compiling apploader..." << std::endl;
	copyStream(appLoaderStream, isoFile, apploaderFileSize);

	std::cout << "Compiling DOL..." << std::endl;
	alignStream(isoFile, static_cast<filesize_t>(getAlignmentBoundary(newDolPosition)));
	copyStream(dolStream, isoFile, dolFileSize);

	std::cout << "Compiling FST..." << std::endl;
	alignStream(isoFile, static_cast<filesize_t>(getAlignmentBoundary(newFstBinPosition)));
	writeToOutputStream(isoFile, fstBinData.data(), fstBinData.size());

	for (const FstEntry &fstEntry : rootDirectory->recursiveGetEntries())
	{
		if (fstEntry.isDirectory())
		{
			const FstDirectory &fstDirectory = static_cast<const FstDirectory &>(fstEntry);

			std::cout << "[0xXXXXXXXX:0xXXXXXX]" << fstDirectory.getFullPath().string() <<
				std::endl;
		}
		else
		{
			const FstFile &fstFile = static_cast<const FstFile &>(fstEntry);

			std::cout << "[0x" << toHexString(fstFile.getPosition(), 8) << ":0x" <<
				toHexString(fstFile.getSize(), 6) << "]" << fstFile.getFullPath().string() << std::endl;

			std::ifstream fstFileStream = openInputBinaryFileWithExceptions(
				fsRootDirectory / fstFile.getFullPath());
			alignStream(isoFile, static_cast<filesize_t>(getAlignmentBoundary(fstFile.getPosition())));
			copyStream(fstFileStream, isoFile, fstFile.getSize());
		}
	}

	alignStream(isoFile, gcIsoFinalAlignment);
}
