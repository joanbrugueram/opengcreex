/*
OpenGCReEx - Open source GameCube ISO extractor / compiler.
Copyright (C) 2015-2016 Joan Bruguera Micó <joanbrugueram@gmail.com>

This file is part of OpenGCReEx.

OpenGCReEx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenGCReEx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenGCReEx.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef OPENGCREEX_COMPILE_HPP
#define OPENGCREEX_COMPILE_HPP

#include "filesystem.hpp"

/**
 * Compile the contents to a Gamecube filesystem to a GameCube ISO.
 * Params:
 *     fsPath: Path where the Gamecube filesystem resides.
 *     isoPath: Path where the output Gamecube ISO should be written.
 *     isDisc2: true to compile the second ISO in a multidisc game, false otherwise
 *              (use false if compiling the first ISO, or the game only has one disc).
 */
void compileGcIso(const filesystem::path &fsPath,
	const filesystem::path &isoPath, bool isDisc2);

#endif // OPENGCREEX_COMPILE_HPP
