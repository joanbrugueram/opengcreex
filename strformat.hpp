/*
OpenGCReEx - Open source GameCube ISO extractor / compiler.
Copyright (C) 2015-2016 Joan Bruguera Micó <joanbrugueram@gmail.com>

This file is part of OpenGCReEx.

OpenGCReEx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenGCReEx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenGCReEx.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef OPENGCREEX_STRFORMAT_HPP
#define OPENGCREEX_STRFORMAT_HPP

#include <string>
#include <sstream>
#include <iomanip>

/**
 * Utility class to format strings inline using std::cout like syntax.
 * Use like MyMethod(StrFormat() << "something" << 123);
 */
class StrFormat
{
	/**
	 * String stream used to format the string internally.
	 */
	std::ostringstream formatter;

public:
	/**
	 * Append a value to the formatted string.
	 * Params:
	 *     - value: Value to append to the formatted string.
	 * Returns: The same object (to use it for chaining).
	 */
	template<typename T>
	StrFormat &operator <<(const T &value)
	{
		formatter << value;
		return *this;
	}

	/**
	 * Get the formatted string.
	 * Returns: The formatted string as an std::string.
	 */
	operator std::string() const
	{
		return formatter.str();
	}
};

/**
 * Format an integral value as a human-readable hexadecimal string.
 * E.g. toHexString(10, 8) == "0x0000000A".
 * Params:
 *     value: The integral value to format as an hexadecimal string.
 *     width: Number of digits to use to format the string.
 * Returns: A human-readable formatted hexadecimal string containing the input value.
 */
template<typename T>
static std::string toHexString(T value, std::streamsize width)
{
	return StrFormat() << std::hex << std::setfill('0') <<
		std::setw(width) << std::uppercase << value;
}

#endif // OPENGCREEX_STRFORMAT_HPP
