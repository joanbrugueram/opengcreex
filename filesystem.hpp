/*
OpenGCReEx - Open source GameCube ISO extractor / compiler.
Copyright (C) 2015-2016 Joan Bruguera Micó <joanbrugueram@gmail.com>

This file is part of OpenGCReEx.

OpenGCReEx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenGCReEx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenGCReEx.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPENGCREEX_FILESYSTEM_HPP
#define OPENGCREEX_FILESYSTEM_HPP

#ifndef USE_EXPERIMENTAL_STD_EXPERIMENTAL_FILESYSTEM_LIBRARY

// Include the boost filesystem library

#define BOOST_FILESYSTEM_NO_DEPRECATED
#include <boost/filesystem.hpp>
namespace filesystem = boost::filesystem;

#else

// Include the standard C++1x filesystem library
// Note that at this moment, support for this is pretty poor and may have bugs

#include <experimental/filesystem>
namespace filesystem = std::experimental::filesystem;

#endif // USE_EXPERIMENTAL_STD_EXPERIMENTAL_FILESYSTEM_LIBRARY

#endif // OPENGCREEX_FILESYSTEM_HPP
