@echo off

REM Build with Boost filesystem (default, more reliable)
cl /EHsc /W4 main.cpp streamutils.cpp extract.cpp compile.cpp fst.cpp /FeGCReEx.exe /IC:\local\boost_1_60_0 /link /LIBPATH:C:\local\boost_1_60_0\lib32-msvc-14.0 boost_system-vc140-mt-1_60.lib boost_filesystem-vc140-mt-1_60.lib
