/*
OpenGCReEx - Open source GameCube ISO extractor / compiler.
Copyright (C) 2015-2016 Joan Bruguera Micó <joanbrugueram@gmail.com>

This file is part of OpenGCReEx.

OpenGCReEx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenGCReEx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenGCReEx.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef OPENGCREEX_NUMERICCHECKS_HPP
#define OPENGCREEX_NUMERICCHECKS_HPP

#include <stdexcept>
#include <type_traits>
#include <limits>
#include "strformat.hpp"

/**
 * Checks if the type T can contain the given value, of type U.
 * See: http://stackoverflow.com/a/17225604
 *
 * Params:
 *     value: The value to check if it can be converted to type T.
 * Returns: True if the given value can be converted to type T safely.
 */
#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-conversion"
#endif // __GNUC__

template <typename T, typename U>
static bool canValueBeConvertedTo(const U value)
{
	return ((value>U(0))==(T(value)>T(0))) && U(T(value))==value;
}

#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif // __GNUC__

/**
 * Casts a value to type T from a source value of type U.
 * On error (cast loses information), throws an exception.
 *
 * Params:
 *     value: The value to be converted from type U to type T.
 * Returns: The value, casted to type T.
 */
template <typename T, typename U>
static T checkedCast(const U value)
{
	if (!canValueBeConvertedTo<T, U>(value))
	{
		throw std::overflow_error(StrFormat() << "Error while casting type " << typeid(U).name() <<
			" to type " << typeid(T).name() << ". Cast is not possible without losing information.");
	}

	return static_cast<T>(value);
}

/**
 * Adds two values of types TSrc1 and TSrc2 to a value of type TDest.
 * On overflow, throws an exception.
 *
 * Params:
 *     value1: The first operand to addition.
 *     value2: The second operand to addition.
 * Returns: The sum of the two values.
 */
template <typename TDest, typename TSrc1, typename TSrc2,
typename std::enable_if<std::is_unsigned<TDest>::value &&
                        std::is_unsigned<TSrc1>::value &&
                        std::is_unsigned<TSrc2>::value>::type* = nullptr>
static TDest checkedAdd(TSrc1 value1, TSrc2 value2)
{
	TDest value1cast = checkedCast<TDest>(value1);
	TDest value2cast = checkedCast<TDest>(value2);

	if (value1cast > std::numeric_limits<TDest>::max() - value2cast)
	{
		throw std::overflow_error(StrFormat() << "Addition to type "
			<< typeid(TDest).name() << " caused an overflow.");
	}

	return value1cast + value2cast;
}

/**
 * Multiplies two values of types TSrc1 and TSrc2 to a value of type TDest.
 * On overflow, throws an exception.
 *
 * Params:
 *     value1: The first operand to multiplication.
 *     value2: The second operand to multiplication.
 * Returns: The product of the two values.
 */
template <typename TDest, typename TSrc1, typename TSrc2,
typename std::enable_if<std::is_unsigned<TDest>::value &&
                        std::is_unsigned<TSrc1>::value &&
                        std::is_unsigned<TSrc2>::value>::type* = nullptr>
static TDest checkedMultiply(TSrc1 value1, TSrc2 value2)
{
	TDest value1cast = checkedCast<TDest>(value1);
	TDest value2cast = checkedCast<TDest>(value2);

	if (value1cast > std::numeric_limits<TDest>::max() / value2cast)
	{
		throw std::overflow_error(StrFormat() << "Multiplication to type "
			<< typeid(TDest).name() << " caused an overflow.");
	}

	return value1cast * value2cast;
}

/**
* Align an integer value to the a multiple of the specified padding boundary.
* https://en.wikipedia.org/wiki/Data_structure_alignment#Computing_padding
* Params:
*     value: The value to be aligned.
*     alignment: The alignment boundary. Must be a power of two.
* Returns: The smallest number bigger or equal than value which is a multiple of alignment.
*/
template<typename T>
static T checkedAlign(T value, T alignment)
{
	if (value > std::numeric_limits<T>::max() - (alignment - 1))
	{
		throw std::overflow_error(StrFormat() << "Alignment of type " <<
			typeid(T).name() << " caused an overflow");
	}

	return (value + alignment - 1) & ~(alignment - 1);
}

#endif // OPENGCREEX_NUMERICCHECKS_HPP
