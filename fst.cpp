/*
OpenGCReEx - Open source GameCube ISO extractor / compiler.
Copyright (C) 2015-2016 Joan Bruguera Micó <joanbrugueram@gmail.com>

This file is part of OpenGCReEx.

OpenGCReEx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenGCReEx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenGCReEx.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <string>
#include <cstdint>
#include <memory>
#include <stdexcept>
#include <map>
#include <functional>
#include <array>
#include <numeric>
#include "fst.hpp"
#include "numericchecks.hpp"
#include "streamutils.hpp"
#include "gcconsts.hpp"

FstEntry::FstEntry(const std::string &pName)
	: parent(nullptr), name(pName)
{
}

void FstEntry::setParent(FstEntry *pParent)
{
	if (parent != nullptr)
	{
		throw std::runtime_error("Reassigning FST parent entry without deassigning first.");
	}

	parent = pParent;
}

FstEntry::~FstEntry()
{
}

FstEntry *FstEntry::getParent()
{
	return parent;
}

const FstEntry *FstEntry::getParent() const
{
	return parent;
}

const std::string &FstEntry::getName() const
{
	return name;
}

filesystem::path FstEntry::getFullPath() const
{
	if (getParent() == nullptr) // Root entry
		return filesystem::path();

	return getParent()->getFullPath() / name;
}

std::vector<std::reference_wrapper<FstEntry>> FstEntry::recursiveGetEntries()
{
	std::vector<std::reference_wrapper<FstEntry>> cont;
	recursivePushEntries(cont);
	return cont;
}

std::vector<std::reference_wrapper<const FstEntry>> FstEntry::recursiveGetEntries() const
{
	std::vector<std::reference_wrapper<const FstEntry>> cont;
	recursivePushEntries(cont);
	return cont;
}

FstDirectory::FstDirectory(const std::string &pName)
	: FstEntry(pName)
{
}

void FstDirectory::add(std::unique_ptr<FstEntry> &&entry)
{
	entry->setParent(this);
	subEntries.push_back(std::move(entry));
}

std::size_t FstDirectory::count() const
{
	return subEntries.size();
}

FstEntry &FstDirectory::getEntry(std::size_t i)
{
	return *subEntries.at(i);
}

const FstEntry &FstDirectory::getEntry(std::size_t i) const
{
	return *subEntries.at(i);
}

std::size_t FstDirectory::recursiveCount() const
{
	return std::accumulate(std::begin(subEntries), std::end(subEntries), static_cast<std::size_t>(1),
		[](std::size_t c, const std::unique_ptr<FstEntry> &e) { return c + e->recursiveCount(); });
}

bool FstDirectory::isDirectory() const
{
	return true;
}

void FstDirectory::recursivePushEntries(std::vector<std::reference_wrapper<FstEntry>> &cont)
{
	cont.push_back(std::ref(*static_cast<FstEntry *>(this)));
	for (std::unique_ptr<FstEntry> &subEntry : subEntries)
		subEntry->recursivePushEntries(cont);
}

void FstDirectory::recursivePushEntries(std::vector<std::reference_wrapper<const FstEntry>> &cont) const
{
	cont.push_back(std::cref(*static_cast<const FstEntry *>(this)));
	for (const std::unique_ptr<FstEntry> &subEntry : subEntries)
		subEntry->recursivePushEntries(cont);
}

FstFile::FstFile(const std::string &pName, std::uint32_t pPosition, std::uint32_t pSize)
	: FstEntry(pName), position(pPosition), size(pSize)
{
}

std::uint32_t FstFile::getPosition() const
{
	return position;
}

void FstFile::setPosition(std::uint32_t pPosition)
{
	position = pPosition;
}

std::uint32_t FstFile::getSize() const
{
	return size;
}

void FstFile::setSize(std::uint32_t pSize)
{
	size = pSize;
}

std::size_t FstFile::recursiveCount() const
{
	return 1;
}

bool FstFile::isDirectory() const
{
	return false;
}

void FstFile::recursivePushEntries(std::vector<std::reference_wrapper<FstEntry>> &cont)
{
	cont.push_back(std::ref(*static_cast<FstEntry *>(this)));
}

void FstFile::recursivePushEntries(std::vector<std::reference_wrapper<const FstEntry>> &cont) const
{
	cont.push_back(std::cref(*static_cast<const FstEntry *>(this)));
}

InvalidFstFileException::InvalidFstFileException(const std::string &what_arg)
	: std::runtime_error(what_arg)
{
}

/**
 * Extract an NUL terminated string from a std::vector<std::uint8_t>.
 * Params:
 *     vector: Vector from which the string should be extracted.
 *     position: Position of the first character in the vector.
 * Returns: The extracted string from the vector, without including the NUL terminator.
 */
static std::string extractStringFromVector(const std::vector<std::uint8_t> &vector, std::size_t position)
{
	// Calculate the size of the string
	std::size_t size = 0;
	while (vector[position+size] != '\0' && position+size < vector.size())
		size++;

	// Ensure that the string is NUL-terminated
	if (position+size == vector.size())
	{
		throw InvalidFstFileException(StrFormat() << "fst.bin string is not NUL terminated.");
	}

	return std::string(reinterpret_cast<const char *>(&vector[position]), size);
}

/**
 * Create an instance of FstEntry from an entry of a FST file.
 * If the entry is a directory, this will also cause all its sub-entries to be read.
 *
 * Params:
 *     fstBinFile: Byte stream corresponding to a fst.bin file.
 *     nameTablePosition: Position inside the fst.bin file where the name table starts.
 *     currentEntryNo: Number of the entry to extract. Use 0 for the root entry.
 *     expectedParentEntryNo: Parent entry number, for consistency check purposes.
 *                            This must be 0 for the root entry.
 * Returns: An instance of FstEntry containing the contents of the entry.
 */
static std::unique_ptr<FstEntry> readFstEntryFromFile(const std::vector<std::uint8_t> &fstBinFile,
	std::size_t nameTablePosition, std::size_t currentEntryNo, std::size_t expectedParentEntryNo)
{
	std::size_t entryPosition = currentEntryNo * gcIsoFstEntrySize;

	std::uint8_t entryType = fstBinFile[entryPosition + gcIsoFstEntryEntryTypeAndNameOffsetPosition];
	std::uint32_t nameOffset = static_cast<std::size_t>(extractBigEndian<std::uint32_t>(fstBinFile.data(),
		entryPosition + gcIsoFstEntryEntryTypeAndNameOffsetPosition) & 0xFFFFFF);

	// Add the component corresponding to this entry to the output path
	std::string entryName;
	if (currentEntryNo != 0) // Not root entry (has a name)
	{
		std::size_t namePosition = checkedAdd<std::size_t>(nameTablePosition, nameOffset);
		if (namePosition > fstBinFile.size())
			throw InvalidFstFileException(StrFormat() << "fst.bin name offset for entry points outside of the file.");
		entryName = extractStringFromVector(fstBinFile, nameTablePosition + nameOffset);
	}
	else // Root entry (which has no name)
	{
		if (nameOffset != 0)
			throw InvalidFstFileException(StrFormat() << "fst.bin name offset for root entry is nonzero.");
	}

	if (entryType == gcIsoFstEntryTypeDirectory)
	{
		std::uint32_t parentEntryNo = extractBigEndian<std::uint32_t>(fstBinFile.data(),
			entryPosition + gcIsoFstDirectoryEntryParentEntryPosition);
		std::uint32_t lastSubEntryNo = extractBigEndian<std::uint32_t>(fstBinFile.data(),
			entryPosition + gcIsoFstDirectoryEntryLastSubEntryPosition);

		// Check consistency
		if (expectedParentEntryNo != parentEntryNo)
			throw InvalidFstFileException(StrFormat() << "Invalid fst.bin file: Directory entry " <<
				currentEntryNo << "'s expected parent number doesn't match the actual parent entry number.");

		if (lastSubEntryNo < currentEntryNo + 1)
			throw InvalidFstFileException(StrFormat() << "Invalid fst.bin file: Directory entry " <<
				currentEntryNo << " has a last subentry number that is lower than minimum expected value.");

		// Iterate over subentries to add them to the current directory
		std::unique_ptr<FstDirectory> directoryEntry(new FstDirectory(entryName));

		for (std::size_t subEntryNo = currentEntryNo + 1; subEntryNo != lastSubEntryNo; )
		{
			std::unique_ptr<FstEntry> subEntry = readFstEntryFromFile(fstBinFile, nameTablePosition,
				subEntryNo, currentEntryNo);
			subEntryNo += subEntry->recursiveCount();
			directoryEntry->add(std::move(subEntry));

			if (subEntryNo > lastSubEntryNo)
			{
				throw InvalidFstFileException(StrFormat() << "Invalid fst.bin file: Directory entry " <<
					currentEntryNo << " surpassed the last entry number during directory extraction.");
			}
		}

		return directoryEntry;
	}
	else if (entryType == gcIsoFstEntryTypeFile)
	{
		std::uint32_t filePosition = extractBigEndian<std::uint32_t>(fstBinFile.data(),
			entryPosition + gcIsoFstFileEntryFilePositionPosition);
		std::uint32_t fileSize = extractBigEndian<std::uint32_t>(fstBinFile.data(),
			entryPosition + gcIsoFstFileEntryFileSizePosition);

		// Generate entry corresponding to this file
		return std::unique_ptr<FstFile>(new FstFile(entryName, filePosition, fileSize));
	}

	throw InvalidFstFileException(StrFormat() << "Invalid fst.bin file: Entry " << currentEntryNo <<
		" is of unknown type.");
}

std::unique_ptr<FstEntry> readFstFile(const std::vector<std::uint8_t> &fstBinFile)
{
	// Check root entry and use it to get the pointer to the nametable
	if (fstBinFile.size() < gcIsoFstEntrySize)
	{
		throw InvalidFstFileException(StrFormat() << "Invalid fst.bin file: File is too small for root entry.");
	}

	std::uint8_t entryType = fstBinFile[gcIsoFstEntryEntryTypeAndNameOffsetPosition];
	if (entryType != gcIsoFstEntryTypeDirectory)
	{
		throw InvalidFstFileException(StrFormat() << "Invalid fst.bin file: Root entry is not a directory.");
	}

	std::uint32_t fstNumEntries = static_cast<std::size_t>(extractBigEndian<std::uint32_t>(
		fstBinFile.data(), gcIsoFstDirectoryEntryLastSubEntryPosition));
	std::size_t fstNameTablePosition = checkedMultiply<std::size_t>(fstNumEntries, gcIsoFstEntrySize);

	if (fstBinFile.size() < fstNameTablePosition)
	{
		throw InvalidFstFileException(StrFormat() << "Invalid fst.bin file: File too small for num. of entries.");
	}

	return readFstEntryFromFile(fstBinFile, fstNameTablePosition, 0, 0);
}

std::vector<std::uint8_t> writeFstFile(const FstEntry &rootDirectory)
{
	std::vector<std::uint8_t> fstBinData;

	// Check given entry to see if it's a root directory
	if (!rootDirectory.isDirectory())
	{
		throw InvalidFstFileException(StrFormat() << "Given root FST directory is not a directory.");
	}

	if (rootDirectory.getParent() != nullptr)
	{
		throw InvalidFstFileException(StrFormat() << "Given root FST directory has a parent.");
	}

	if (rootDirectory.getName() != "")
	{
		throw InvalidFstFileException(StrFormat() << "Given root FST directory has a name.");
	}

	// Get the list of entries in the FST. This is in the correct order for writing.
	std::vector<std::reference_wrapper<const FstEntry>> entries = rootDirectory.recursiveGetEntries();

	// Generate a table containing the index of every entry in the FST, for parent entry lookups
	std::map<const FstEntry *, std::size_t> entryIndexes;
	for (size_t i = 0; i < entries.size(); i++)
		entryIndexes.insert(std::make_pair(&entries[i].get(), i));

	// Write entry table
	std::size_t currentNameTablePosition = 0;

	for (const FstEntry &fstEntry : entries)
	{
		std::array<std::uint8_t, gcIsoFstEntrySize> entryBytes;

		std::uint8_t entryType = fstEntry.isDirectory() ? gcIsoFstEntryTypeDirectory
		                                                : gcIsoFstEntryTypeFile;

		writeBigEndian<std::uint32_t>(entryBytes.data(), gcIsoFstEntryEntryTypeAndNameOffsetPosition,
			(static_cast<std::uint32_t>(entryType) << 24U) | checkedCast<std::uint32_t>(currentNameTablePosition));

		if (fstEntry.isDirectory())
		{
			const FstDirectory &fstDirectory = static_cast<const FstDirectory &>(fstEntry);

			std::size_t parentEntryNo = (fstDirectory.getParent() != nullptr) ? entryIndexes[fstDirectory.getParent()] : 0;
			std::size_t lastSubEntryNo = entryIndexes[&fstDirectory] + fstDirectory.recursiveCount();

			writeBigEndian<std::uint32_t>(entryBytes.data(), gcIsoFstDirectoryEntryParentEntryPosition,
				checkedCast<std::uint32_t>(parentEntryNo));
			writeBigEndian<std::uint32_t>(entryBytes.data(), gcIsoFstDirectoryEntryLastSubEntryPosition,
				checkedCast<std::uint32_t>(lastSubEntryNo));
		}
		else
		{
			const FstFile &fstFile = static_cast<const FstFile &>(fstEntry);

			writeBigEndian<std::uint32_t>(entryBytes.data(), gcIsoFstFileEntryFilePositionPosition,
				fstFile.getPosition());
			writeBigEndian<std::uint32_t>(entryBytes.data(), gcIsoFstFileEntryFileSizePosition,
				fstFile.getSize());
		}

		fstBinData.insert(fstBinData.end(), entryBytes.begin(), entryBytes.end());

		// Advance name table position pointer
		if (&fstEntry != &rootDirectory) // Root directory has no name
		{
			currentNameTablePosition += fstEntry.getName().size() + 1;
			if (currentNameTablePosition > 0xFFFFFF)
			{
				throw InvalidFstFileException("Error creating fst.bin file, name table is too big.");
			}
		}
	}

	// Write entry name table
	for (const FstEntry &fstEntry : entries)
	{
		if (&fstEntry != &rootDirectory) // Root directory has no name
		{
			const std::string &name = fstEntry.getName();
			fstBinData.insert(fstBinData.end(), reinterpret_cast<const std::uint8_t *>(name.data()),
				reinterpret_cast<const std::uint8_t *>(name.data() + name.size() + 1));
		}
	}

	return fstBinData;
}
