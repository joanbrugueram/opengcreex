/*
OpenGCReEx - Open source GameCube ISO extractor / compiler.
Copyright (C) 2015-2016 Joan Bruguera Micó <joanbrugueram@gmail.com>

This file is part of OpenGCReEx.

OpenGCReEx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenGCReEx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenGCReEx.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <fstream>
#include <limits>
#include <iostream>
#include <cstdint>
#include <vector>
#include <array>
#include "filesystem.hpp"
#include "streamutils.hpp"
#include "numericchecks.hpp"

std::ifstream openInputBinaryFileWithExceptions(const filesystem::path &path)
{
	std::ifstream stream;
	stream.open(path.string(), std::ifstream::binary);
	if (!stream.is_open())
	{
		throw std::ios_base::failure(StrFormat() << "Error opening the input file '"
			<< path.string() << "'.");
	}
	stream.exceptions(std::ifstream::eofbit | std::ifstream::failbit | std::ifstream::badbit);
	return stream;
}

void setInputStreamPosition(std::istream &stream, fileoffset_t position)
{
	if (!canValueBeConvertedTo<std::streamoff>(position))
	{
		throw std::runtime_error(StrFormat() << __func__ << ": Position=" << position << " argument "
			"is too big for std::streampos. Compile the program with big file support?");
	}

	stream.seekg(static_cast<std::streamoff>(position));
}

filesize_t getStreamSize(std::istream &stream)
{
	std::streampos oldPos = stream.tellg();
	stream.seekg(0, std::ifstream::end);
	std::streampos size = stream.tellg();
	stream.seekg(oldPos);

	if (!canValueBeConvertedTo<filesize_t>(size))
	{
		throw std::runtime_error(StrFormat() << __func__ << ": Size=" << size << " argument "
			"is too big for filesize_t. Compile the program with big file support?");
	}

	return static_cast<filesize_t>(size);
}

void readFromInputStream(std::istream &stream, std::uint8_t *buffer, std::size_t size)
{
	if (!canValueBeConvertedTo<std::streamsize>(size))
	{
		throw std::runtime_error(StrFormat() << __func__ << ": Size=" << size << " argument "
			"is too big for std::streamsize. Compile the program with big file support?");
	}

	stream.read(reinterpret_cast<char *>(buffer), static_cast<std::streamsize>(size));
}

std::ofstream openOutputBinaryFileWithExceptions(const filesystem::path &path)
{
	std::ofstream stream;
	stream.open(path.string(), std::ofstream::binary);
	if (!stream.is_open())
	{
		throw std::ios_base::failure(StrFormat() << "Error opening the output file '"
			<< path.string() << "'.");
	}
	stream.exceptions(std::ofstream::eofbit | std::ofstream::failbit | std::ofstream::badbit);
	return stream;
}

void writeToOutputStream(std::ostream &stream, const std::uint8_t *buffer, std::size_t size)
{
	if (!canValueBeConvertedTo<std::streamsize>(size))
	{
		throw std::runtime_error(StrFormat() << __func__ << ": Size=" << size << " argument "
			"is too big for std::streamsize. Compile the program with big file support?");
	}

	stream.write(reinterpret_cast<const char *>(buffer), static_cast<std::streamsize>(size));
}

static const std::array<std::uint8_t, 0x2000> zeroBuffer = { 0 };

void alignStream(std::ostream &stream, filesize_t alignment)
{
	filesize_t currSize = checkedCast<filesize_t>(stream.tellp());
	filesize_t paddingAmount = checkedAlign(currSize, alignment) - currSize;

	filesize_t remain = paddingAmount;
	while (remain != 0)
	{
		size_t take = static_cast<std::size_t>(std::min(remain, static_cast<filesize_t>(zeroBuffer.size())));
		writeToOutputStream(stream, zeroBuffer.data(), take);
		remain -= take;
	}
}

std::vector<std::uint8_t> readFileToMemory(const filesystem::path &path)
{
	// Open input stream
	std::ifstream inFile = openInputBinaryFileWithExceptions(path);
	filesize_t size = getStreamSize(inFile);

	// Allocate vector and read file contents
	if (!canValueBeConvertedTo<std::size_t>(size))
	{
		throw std::runtime_error(StrFormat() << __func__ << ": Size=" << size << " argument "
			"is too big for std::size_t. Compile the program with big file support?");
	}

	std::vector<std::uint8_t> inFileData(static_cast<std::size_t>(size));
	readFromInputStream(inFile, inFileData.data(), inFileData.size());
	return inFileData;
}

void extractToFile(std::istream &stream, fileoffset_t position, filesize_t size,
	const filesystem::path &outPath)
{
	std::ofstream outFile = openOutputBinaryFileWithExceptions(outPath);
	setInputStreamPosition(stream, position);
	copyStream(stream, outFile, size);
}

void copyStream(std::istream &inStream, std::ostream &outStream, filesize_t size)
{
	std::array<std::uint8_t, 0x2000> buffer;
	filesize_t remain = size;
	while (remain != 0)
	{
		size_t take = static_cast<std::size_t>(std::min(remain, static_cast<filesize_t>(buffer.size())));
		readFromInputStream(inStream, buffer.data(), take);
		writeToOutputStream(outStream, buffer.data(), take);
		remain -= take;
	}
}
