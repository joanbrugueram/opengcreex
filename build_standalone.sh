#!/bin/bash

# Build with C++1x experiemental filesystem (possibly buggy)
g++ -pedantic -Wall -Wextra -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wdisabled-optimization -Wformat=2 -Winit-self -Wlogical-op -Wmissing-declarations -Wmissing-include-dirs -Wnoexcept -Wold-style-cast -Woverloaded-virtual -Wredundant-decls -Wshadow -Wsign-conversion -Wsign-promo -Wstrict-null-sentinel -Wstrict-overflow=5 -Wswitch-default -Wundef -Werror -Wno-unused main.cpp streamutils.cpp extract.cpp compile.cpp fst.cpp --std=c++11 -DUSE_EXPERIMENTAL_STD_EXPERIMENTAL_FILESYSTEM_LIBRARY -lstdc++fs -o GCReEx
