/*
OpenGCReEx - Open source GameCube ISO extractor / compiler.
Copyright (C) 2015-2016 Joan Bruguera Micó <joanbrugueram@gmail.com>

This file is part of OpenGCReEx.

OpenGCReEx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenGCReEx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenGCReEx.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
#include <string>
#include <cstdint>
#include <fstream>
#include <stdexcept>
#include <array>
#include <limits>
#include <algorithm>
#include <cctype>
#include "filesystem.hpp"
#include <vector>
#include "gcconsts.hpp"
#include "extract.hpp"
#include "streamutils.hpp"
#include "strformat.hpp"
#include "numericchecks.hpp"
#include "fst.hpp"

/**
 * Extract the contents of a chunk of the specified stream to a file.
 * Params:
 *     stream: Stream from which the chunk should be extracted.
 *     position: Position inside the stream where the chunk begins.
 *     size: Size of the chunk inside the stream.
 *     outPath: Path where the extracted chunk should be written.
 * Returns: true if the file has been extracted, false if the file already exists.
 */
static bool extractToFileIfNotExists(std::istream &stream, fileoffset_t position, filesize_t size,
	const filesystem::path &outPath)
{
	if (filesystem::is_regular_file(outPath))
		return false;
	extractToFile(stream, position, size, outPath);
	return true;
}

/**
* Extract a Gamecube apploader from a GameCube ISO.
* Params:
*     stream: Stream from which the apploader should be extracted.
*     position: Position inside the stream where the apploader file begins.
*     outPath: Path where the extracted apploader should be written.
*/
static void extractApploader(std::istream &stream, fileoffset_t position,
	const filesystem::path &outPath)
{
	// Read apploader header (we need this to figure the apploader file size)
	std::array<std::uint8_t, gcIsoAppLoaderHeaderSize> appLoaderHeader;
	setInputStreamPosition(stream, position);
	readFromInputStream(stream, appLoaderHeader.data(), appLoaderHeader.size());

	// Compute apploader size from header fields
	std::uint32_t appLoaderBinarySize = extractBigEndian<std::uint32_t>(
		appLoaderHeader.data(), gcIsoAppLoaderHeaderBinarySizePosition);
	std::uint32_t appLoaderTrailerSize = extractBigEndian<std::uint32_t>(
			appLoaderHeader.data(), gcIsoAppLoaderHeaderTrailerSizePosition);
	filesize_t appLoaderHeaderAndBinarySize = checkedAdd<filesize_t>(gcIsoAppLoaderHeaderSize,
		checkedAdd<filesize_t>(appLoaderBinarySize, appLoaderTrailerSize));

	// Extract apploader to file
	std::cout << "Apploader size:" << appLoaderHeaderAndBinarySize << std::endl;

	bool appLoaderCreated = extractToFileIfNotExists(
		stream, position, appLoaderHeaderAndBinarySize, outPath);
	std::cout << "Extracting apploader..." << (!appLoaderCreated ? " -> ALREADY EXISTS" : "") << std::endl;
}

/**
 * Extract a Gamecube executable (DOL file) from a GameCube ISO.
 * Params:
 *     stream: Stream from which the DOL should be extracted.
 *     dolPosition: Position inside the stream where the DOL file begins.
 *     outPath: Path where the extracted DOL should be written.
 */
static void extractDol(std::istream &stream, fileoffset_t dolPosition,
	const filesystem::path &outPath)
{
	// Read DOL header (we need this to figure the DOL file size)
	std::array<std::uint8_t, gcIsoDolHeaderSize> dolHeader;
	setInputStreamPosition(stream, dolPosition);
	readFromInputStream(stream, dolHeader.data(), dolHeader.size());

	// Compute DOL size from header fields
	filesize_t dolSize = gcIsoDolHeaderSize;
	for (std::size_t i = 0; i < gcIsoDolNumTextSections; i++)
	{
		dolSize = checkedAdd<filesize_t>(dolSize,
			extractBigEndian<std::uint32_t>(dolHeader.data(), gcIsoDolTextSectionSizesPosition + 4 * i));
	}
	for (std::size_t i = 0; i < gcIsoDolNumDataSections; i++)
	{
		dolSize = checkedAdd<filesize_t>(dolSize,
			extractBigEndian<std::uint32_t>(dolHeader.data(), gcIsoDolDataSectionSizesPosition + 4 * i));
	}
	dolSize = checkedAdd<filesize_t>(dolSize,
		extractBigEndian<std::uint32_t>(dolHeader.data(), gcIsoDolBssSizePosition));

	// Extract DOL to file
	std::cout << "DOL offset :0x" << toHexString(dolPosition, 8) << std::endl;
	std::cout << "DOL size   :0x" << toHexString(dolSize, 8) << std::endl;

	bool dolCreated = extractToFileIfNotExists(stream, dolPosition, dolSize, outPath);
	std::cout << "Extracting DOL..." << (!dolCreated ? " -> ALREADY EXISTS" : "") << std::endl;
}

/**
 * Extracting the contents of a Gamecube filesystem from a fst.bin root directory entry.
 *
 * Params:
 *     rootDirectory: Root directory in the fst.bin filesystem we are currently extracting.
 *     srcStream: Stream containing the contents of the file to extract (i.e. Gamecube ISO).
 *     fsPath: Base filesystem output path.
 */
static void extractFileSystem(const FstEntry &rootDirectory,
	std::istream &srcStream, const filesystem::path &fsPath)
{
	for (const FstEntry &fstEntry : rootDirectory.recursiveGetEntries())
	{
		filesystem::path entryPath = fsPath / fstEntry.getFullPath();

		if (fstEntry.isDirectory())
		{
			const FstDirectory &fstDirectory = static_cast<const FstDirectory &>(fstEntry);

			// Create directory corresponding to this entry
			bool created = filesystem::create_directory(entryPath);
			std::cout << "[0xXXXXXXXX:0xXXXXXX]" << fstDirectory.getFullPath().string() <<
				(!created ? " -> ALREADY EXISTS" : "") << std::endl;
		}
		else
		{
			const FstFile &fstFile = static_cast<const FstFile &>(fstEntry);

			// Extract contents of this entry to a file
			bool created = extractToFileIfNotExists(srcStream, fstFile.getPosition(), fstFile.getSize(), entryPath);

			std::cout << "[0x" << toHexString(fstFile.getPosition(), 8) << ":0x" <<
				toHexString(fstFile.getSize(), 6) << "]" << fstFile.getFullPath().string() <<
				(!created ? " -> ALREADY EXISTS" : "") << std::endl;
		}
	}
}

void extractGcIso(const filesystem::path &isoPath, const filesystem::path &fsPath)
{
	std::ifstream isoFile = openInputBinaryFileWithExceptions(isoPath);

	// Read Gamecube ISO header and check valid magic number
	std::array<std::uint8_t, gcIsoHeaderSize> gcIsoHeader;
	setInputStreamPosition(isoFile, gcIsoHeaderPosition);
	readFromInputStream(isoFile, gcIsoHeader.data(), gcIsoHeader.size());

	std::uint32_t magic = extractBigEndian<std::uint32_t>(
		gcIsoHeader.data(),gcIsoHeaderMagicPosition);
	if (magic != gcIsoHeaderMagic)
		throw std::runtime_error("Invalid GameCube ISO magic number.");

	bool isDisc2 = (gcIsoHeader[gcIsoHeaderDiscIdPosition] != 0);

	// Create output directory structure
	// We don't need to create the output directory for the filesystem
	// because it will be created later when extracting the root filesystem entry
	std::string gameId(gcIsoHeader.begin() + 0, gcIsoHeader.begin() + 6);
	if (std::any_of(gameId.begin(), gameId.end(),
		[](char c) { return !std::isupper(c) && !std::isdigit(c); }))
	{
		throw std::runtime_error("GameCube ISO GameId contains invalid characters.");
	}

	filesystem::path baseOutputDirectory = fsPath / gameId;
	filesystem::path fsSysDirectory = baseOutputDirectory / gcFsSysDirectoryName;
	filesystem::path fsRootDirectory = baseOutputDirectory / gcFsRootDirectoryName;

	if (!filesystem::create_directory(baseOutputDirectory) ||
	    !filesystem::create_directory(fsSysDirectory))
	{
		std::cout << "NOTE: Output directory already exists. You may have already extracted this ISO."
			<< std::endl;
	}

	// Extract disc header
	bool discHeaderCreated = extractToFileIfNotExists(isoFile, gcIsoHeaderPosition, gcIsoHeaderSize,
		fsSysDirectory / getGcIsoHeaderFileName(isDisc2));
	std::cout << "Extracting disc header..." << (!discHeaderCreated ? " -> ALREADY EXISTS" : "") << std::endl;

	// Extract bi2
	bool bi2Created = extractToFileIfNotExists(
		isoFile, gcIsoBi2Position, gcIsoBi2Size, fsSysDirectory / gcIsoBi2FileName);
	std::cout << "Extracting bi2..." << (!bi2Created ? " -> ALREADY EXISTS" : "") << std::endl;

	// Extract apploader
	extractApploader(isoFile, gcIsoAppLoaderPosition, fsSysDirectory / gcIsoAppLoaderFileName);

	// Extract DOL
	std::uint32_t dolPosition = extractBigEndian<std::uint32_t>(
		gcIsoHeader.data(), gcIsoHeaderDolPositionPosition);
	extractDol(isoFile, dolPosition, fsSysDirectory / gcIsoDolFileName);

	// Extract FST
	std::uint32_t fstBinPosition = extractBigEndian<std::uint32_t>(
		gcIsoHeader.data(), gcIsoHeaderFstPositionPosition);
	std::uint32_t fstBinSize = static_cast<std::size_t>(extractBigEndian<std::uint32_t>(
		gcIsoHeader.data(), gcIsoHeaderFstSizePosition));

	std::cout << "FST offset :0x" << toHexString(fstBinPosition, 8) << std::endl;
	std::cout << "FST size   :0x" << toHexString(fstBinSize, 8) << std::endl;
	bool fstCreated = extractToFileIfNotExists(isoFile, fstBinPosition, fstBinSize,
		fsSysDirectory / getGcIsoFstFileName(isDisc2));
	std::cout << "Extracting FST..." << (!fstCreated ? " -> ALREADY EXISTS" : "") << std::endl;

	// Read FST content to extract files
	std::vector<std::uint8_t> fstBinData(fstBinSize);
	setInputStreamPosition(isoFile, fstBinPosition);
	readFromInputStream(isoFile, fstBinData.data(), fstBinData.size());

	std::unique_ptr<FstEntry> rootDirectory = readFstFile(fstBinData);

	std::cout << "FST entries:" << rootDirectory->recursiveCount() << std::endl;
	std::cout << "FST names  :0x" << toHexString(
		fstBinPosition + rootDirectory->recursiveCount() * gcIsoFstEntrySize, 8) << std::endl;
	std::cout << std::endl;

	extractFileSystem(*rootDirectory, isoFile, fsRootDirectory);
}
