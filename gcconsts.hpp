/*
OpenGCReEx - Open source GameCube ISO extractor / compiler.
Copyright (C) 2015-2016 Joan Bruguera Micó <joanbrugueram@gmail.com>

This file is part of OpenGCReEx.

OpenGCReEx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenGCReEx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenGCReEx.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef OPENGCREEX_GCCONSTS_HPP
#define OPENGCREEX_GCCONSTS_HPP

#include <string>
#include "streamutils.hpp"

const std::size_t gcIsoFinalAlignment = 0x8000; // 32 KB

// Output directory structure
const std::string gcFsSysDirectoryName = "sys";
const std::string gcFsRootDirectoryName = "root";

// ISO header
const fileoffset_t gcIsoHeaderPosition = 0x00000000;
const std::size_t gcIsoHeaderSize = 0x00000440;

const std::size_t gcIsoHeaderDiscIdPosition = 0x06;

const std::size_t gcIsoHeaderMagicPosition = 0x1C;
const std::uint32_t gcIsoHeaderMagic = 0xC2339F3D;

const std::size_t gcIsoHeaderDolPositionPosition = 0x420;

const std::size_t gcIsoHeaderFstPositionPosition = 0x424;
const std::size_t gcIsoHeaderFstSizePosition = 0x428;
const std::size_t gcIsoHeaderFstMaxSizePosition = 0x42C;

// TODO remove unused function warning on include from fst.cpp
static std::string getGcIsoHeaderFileName(bool isDisc2)
{
	return !isDisc2 ? "boot.bin" : "boot2.bin";
}

// ISO header information (bi2.bin)
const fileoffset_t gcIsoBi2Position = 0x00000440;
const std::size_t gcIsoBi2Size = 0x00002000;

const std::string gcIsoBi2FileName = "bi2.bin";

// Apploader (apploader.img)
const fileoffset_t gcIsoAppLoaderPosition = 0x00002440;
const std::size_t gcIsoAppLoaderHeaderSize = 0x20;

const std::size_t gcIsoAppLoaderHeaderBinarySizePosition = 0x14;
const std::size_t gcIsoAppLoaderHeaderTrailerSizePosition = 0x18;

const std::string gcIsoAppLoaderFileName = "apploader.img";

// DOL executable (main.dol)
const std::size_t gcIsoDolHeaderSize = 0x100;

const std::size_t gcIsoDolTextSectionSizesPosition = 0x90;
const std::size_t gcIsoDolNumTextSections = 7;

const std::size_t gcIsoDolDataSectionSizesPosition = 0xAC;
const std::size_t gcIsoDolNumDataSections = 11;

const std::size_t gcIsoDolBssSizePosition = 0xD4;

const std::string gcIsoDolFileName = "main.dol";

// Filesystem table (fst.bin)
const std::size_t gcIsoFstEntrySize = 0x0C;

const std::size_t gcIsoFstEntryEntryTypeAndNameOffsetPosition = 0x00;
const std::size_t gcIsoFstDirectoryEntryParentEntryPosition = 0x04;
const std::size_t gcIsoFstDirectoryEntryLastSubEntryPosition = 0x08;
const std::size_t gcIsoFstFileEntryFilePositionPosition = 0x04;
const std::size_t gcIsoFstFileEntryFileSizePosition = 0x08;

const std::uint8_t gcIsoFstEntryTypeFile = 0x00;
const std::uint8_t gcIsoFstEntryTypeDirectory = 0x01;

// TODO remove unused function warning on include from fst.cpp
static std::string getGcIsoFstFileName(bool isDisc2)
{
	return !isDisc2 ? "fst.bin" : "fst2.bin";
}

#endif // OPENGCREEX_GCCONSTS_HPP
