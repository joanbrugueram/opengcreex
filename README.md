# OpenGCReEx

**WARNING:** This program is in early development stages. While basic functionality should work, do not be surprised if you find bugs, unexpected behavior or missing features.

## Introduction

OpenGCReEx is a tool to extract the content of GameCube ISOs/GCMs to a folder containing their individual files, and recompile those folders to their original ISOs/GCMs.

It is functionally similar to a tool called GCReEx, which is a closed-source tool which offers a similar functionality. It is also functionally similar to tools such as Gamecube ISO Tool, GC-Tool, or wit. OpenGCReEx was born as a portable open-source command line tool with full GC support.

## Usage

OpenGCReEx must be used from the command line. There is no graphical user interface available.

### Extract a single disc game

Use the following command:
```
OpenGCReEx -x disc.iso
```

This will create a directory in the same directory as OpenGCReEx, named like the Game ID code, which will contain the extracted ISO contents.

You can also use the command:
```
OpenGCReEx -x disc.iso /path/to/output/folder
```

To specify the output folder where the extracted ISO contents should be written.

### Extract a multi disc game

Use the following commands:

```
OpenGCReEx -x disc1.iso
OpenGCReEx -x disc2.iso
```

This will create a directory in the same directory as OpenGCReEx, which will contain the combined extracted ISO contents.

As in the previous case, you can also specify the output directory as an extra parameter.

### Compile a single disc game

Use the following command:

```
OpenGCReEx -c /path/to/source/folder
```

This will create a ISO in the same directory as OpenGCReEx, named `out.iso`, which will contain the compiled ISO contents.


You can also use the command:
```
OpenGCReEx -c /path/to/source/folder /path/to/output/game.iso
```

To specify the output path where the compiled ISO contents should be written.

### Compile a multi disc game

Use the following command:

```
OpenGCReEx -c /path/to/source/folder
OpenGCReEx -c2 /path/to/source/folder
```

This will create two ISO in the same directory as OpenGCReEx, named `out.iso` and `out.iso2`, which will contain the compiled ISO contents for both discs.

As in the previous case, you can also specify the output ISO path as an extra parameter.

**Important:** If you want to compile a multi disc game, you must first extract *both* discs to a *common* folder, then do the compilation based on that folder containing both discs. Extracting only a single disc, or each disc to a different folder, can easily lead to non working results (rebuilding a multi disc game requires that OpenGCReEx can access the data of *both* discs due to the way the Gamecube ISO header layout works).

## Output directory structure

After extracting the contents of a Gamecube ISO, the directory layout is as follows:

* [Game ID]
    * sys
        * [boot.bin (single disc or first disc) / boot2.bin (second disc)](http://hitmen.c02.at/files/yagcd/yagcd/chap13.html#sec13.1): Disc header.
        * [bi2.bin](http://hitmen.c02.at/files/yagcd/yagcd/chap13.html#sec13.2): Disc header information.
        * [apploader.img](http://www.gc-forever.com/wiki/index.php?title=Apploader): First code executed, to load the FST/DOL.
        * [main.dol](http://hitmen.c02.at/files/yagcd/yagcd/chap14.html#sec14.2): Main executable binary.
        * [fst.bin (single disc or first disc) / fst2.bin](http://hitmen.c02.at/files/yagcd/yagcd/chap13.html#sec13.4): Filesystem layout.
    * root
        * Game specific files / folders go here.

Notes:

* Only the files and directories specified in the fst.bin file will be included in the compiled ISO result. Therefore, adding or removing files or directories requires you to edit the fst.bin file manually or with an external tool. Support for adding or removing files or directories will be added in a future release.

* When compiling an ISO, some fields in the disc header and the filesystem layout files will be updated to reflect the new positions of the distinct binaries in the ISO file. Therefore, do not expect those fields to be mantained as-is after compilation.

### TODO List

* Add support for adding / removing files to ISO (converting fst.bin to text and back).

* Add unit tests.

* Potential code improvements.

### Building OpenGCReEx

OpenGCReEx is an standard C++11 console application. However, it needs either the [experimental C++1x filesystem library](http://en.cppreference.com/w/cpp/experimental/fs) or the [Boost filesystem](http://www.boost.org/doc/libs/1_60_0/libs/filesystem/doc/index.htm) library to build.

* **Standalone build (with experimental C++1x filesystem library)**: If your compiler offers good support for this, this is the easiest option (it works on Visual C++ 2015 and g++ 5.3.0), since you don't need any external dependency.

    Simply run `build_standalone.bat` (Visual C++ 2015, Windows) or `build_standalone.sh` (g++ 5.3.0+, Linux and similar).

    WARNING: It appears that the current g++ implementation of `std::experimental::filesystem::create_directory` is buggy and throws an exception when it should return false. At the moment, prefer building with Boost on Linux.

* **Build with Boost filesystem library:** This should work with any compiler with C++11 support. You will need to install Boost on your system (e.g. using the [binaries for Windows](http://sourceforge.net/projects/boost/files/boost-binaries/) or from your distro's repository).

    Once installed, run `build_boost.bat` (Visual C++, Windows) or `build_boost.sh` (g++, Linux and similar).

    NOTE: On Windows, you may need to tweak the paths to Boost depending on your setup options or Visual C++ version.

### Licensing
OpenGCReEx is licensed under the [GNU Public License (GPL)](http://www.gnu.org/licenses/).
