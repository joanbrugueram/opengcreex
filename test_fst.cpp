#include <boost/test/unit_test.hpp>
#include "fst.hpp"

/**
 * Test with a empty fst.bin file (no bytes).
 * Should fail because every fst.bin should have a complete root entry.
 */
BOOST_AUTO_TEST_CASE( test_read_fst_empty )
{
	const std::vector<std::uint8_t> fstBytes = {};
	BOOST_REQUIRE_THROW(readFstFile(fstBytes), InvalidFstFileException);
}

/**
 * Test with a small (<12 bytes) fst.bin file.
 * Should fail because every fst.bin should have a complete root entry.
 */
BOOST_AUTO_TEST_CASE( test_read_fst_no_root )
{
	const std::vector<std::uint8_t> fstBytes = {
		0x01 /* Type flag */, 0x00, 0x00, 0x00 /* String table offset */,
		0x00, 0x00, 0x00, 0x00, /* Parent entry number */
		0x00, 0x00, 0x00 /* Last entry number not complete (INVALID) */
	};

	BOOST_REQUIRE_THROW(readFstFile(fstBytes), InvalidFstFileException);
}

/**
 * Test with a minimalistic (only root entry) fst.bin file.
 */
BOOST_AUTO_TEST_CASE( test_read_fst_valid_minimalistic )
{
	const std::vector<std::uint8_t> fstBytes = {
		0x01 /* Type flag */, 0x00, 0x00, 0x00 /* String table offset */,
		0x00, 0x00, 0x00, 0x00, /* Parent entry number */
		0x00, 0x00, 0x00, 0x01 /* Last entry number */ };

	std::unique_ptr<FstEntry> fstRoot = readFstFile(fstBytes);
	BOOST_REQUIRE(fstRoot.get());
	BOOST_REQUIRE(!fstRoot->getParent());
	BOOST_REQUIRE_EQUAL(fstRoot->getName(), "");
	BOOST_REQUIRE_EQUAL(fstRoot->getFullPath(), "");
	BOOST_REQUIRE_EQUAL(fstRoot->isDirectory(), true);
	BOOST_REQUIRE_EQUAL(fstRoot->recursiveCount(), 1);
	FstDirectory &fstRootDirectory = static_cast<FstDirectory &>(*fstRoot);
	BOOST_REQUIRE_EQUAL(fstRootDirectory.count(), 0);

	std::vector<std::reference_wrapper<FstEntry>> rootSubEntries = fstRootDirectory.recursiveGetEntries();
	BOOST_REQUIRE_EQUAL(rootSubEntries.size(), 1);
	BOOST_REQUIRE_EQUAL(&rootSubEntries[0].get(), &fstRootDirectory);
}

/**
 * Test with a fst.bin file where the root entry is a file.
 */
BOOST_AUTO_TEST_CASE( test_read_fst_root_is_file )
{
	const std::vector<std::uint8_t> fstBytes = {
		0x00 /* Type flag (INVALID) */, 0x00, 0x00, 0x00 /* String table offset */,
		0x00, 0x00, 0x00, 0x00, /* File offset */
		0x00, 0x00, 0x00, 0x01 /* File size */ };

	BOOST_REQUIRE_THROW(readFstFile(fstBytes), InvalidFstFileException);
}

/**
 * Test with a fst.bin file where the root entry string table offset is nonzero.
 * The root entry has no name and the offset should be zero.
 * This shouldn't happen on well-formed fst.bin files.
 */
BOOST_AUTO_TEST_CASE( test_read_fst_root_has_name )
{
	const std::vector<std::uint8_t> fstBytes = {
		0x01 /* Type flag */, 0x00, 0x00, 0x01 /* String table offset (INVALID) */,
		0x00, 0x00, 0x00, 0x00, /* Parent entry number */
		0x00, 0x00, 0x00, 0x01, /* Last entry number */
		'\0', 't', 'e', 's', 't', '\0' /* Nametable */ };

	BOOST_REQUIRE_THROW(readFstFile(fstBytes), InvalidFstFileException);
}

/**
 * Test with a fst.bin file where the root entry has a nonzero parent entry.
 */
BOOST_AUTO_TEST_CASE( test_read_fst_invalid_root_parent )
{
	const std::vector<std::uint8_t> fstBytes = {
		0x01 /* Type flag */, 0x00, 0x00, 0x00 /* String table offset */,
		0x00, 0x00, 0x00, 0xFF, /* Parent entry number (INVALID) */
		0x00, 0x00, 0x00, 0x01 /* Last entry number */ };

	BOOST_REQUIRE_THROW(readFstFile(fstBytes), InvalidFstFileException);
}

/**
 * Test with a fst.bin file where the total number of entries specified
 * in the root entry is too big for the real number of entries.
 */
BOOST_AUTO_TEST_CASE( test_read_fst_too_many_entries )
{
	const std::vector<std::uint8_t> fstBytes = {
		0x01 /* Type flag */, 0x00, 0x00, 0x00 /* String table offset */,
		0x00, 0x00, 0x00, 0x00, /* Parent entry number */
		0x22, 0x22, 0x22, 0x22 /* Last entry number (INVALID) */ };

	BOOST_REQUIRE_THROW(readFstFile(fstBytes), InvalidFstFileException);
}

/**
 * Test with a fst.bin with a few regular entries (directories and files).
 * Root:
 *   testfile1
 *   testfolder:
 *     testfile2
 *   testfile3
 */
BOOST_AUTO_TEST_CASE( test_read_fst_valid_regular )
{
	const std::vector<std::uint8_t> fstBytes = {
		/* Root */
		0x01 /* Type flag */, 0x00, 0x00, 0x00 /* String table offset */,
		0x00, 0x00, 0x00, 0x00, /* Parent entry number */
		0x00, 0x00, 0x00, 0x05, /* Last entry number */
		/* testfile1 */
		0x00 /* Type flag */, 0x00, 0x00, 0x00 /* String table offset */,
		0x11, 0x11, 0x11, 0x11, /* File offset */
		0x22, 0x22, 0x22, 0x22, /* File size */
		/* testfolder */
		0x01 /* Type flag */, 0x00, 0x00, 0x0A /* String table offset */,
		0x00, 0x00, 0x00, 0x00, /* Parent entry number */
		0x00, 0x00, 0x00, 0x04, /* Last entry number */
		/* testfile2 */
		0x00 /* Type flag */, 0x00, 0x00, 0x15 /* String table offset */,
		0x33, 0x33, 0x33, 0x33, /* Parent entry number */
		0x44, 0x44, 0x44, 0x44, /* Last entry number */
		/* testfile3 */
		0x00 /* Type flag */, 0x00, 0x00, 0x1F /* String table offset */,
		0x55, 0x55, 0x55, 0x55, /* Parent entry number */
		0x66, 0x66, 0x66, 0x66, /* Last entry number */
		't', 'e', 's', 't', 'f', 'i', 'l', 'e', '1', '\0',
		't', 'e', 's', 't', 'f', 'o', 'l', 'd', 'e', 'r', '\0',
		't', 'e', 's', 't', 'f', 'i', 'l', 'e', '2', '\0',
		't', 'e', 's', 't', 'f', 'i', 'l', 'e', '3', '\0'
	};

	std::unique_ptr<FstEntry> fstRoot = readFstFile(fstBytes);
	BOOST_REQUIRE(fstRoot.get());
	BOOST_REQUIRE(!fstRoot->getParent());
	BOOST_REQUIRE_EQUAL(fstRoot->getName(), "");
	BOOST_REQUIRE_EQUAL(fstRoot->getFullPath(), "");
	BOOST_REQUIRE_EQUAL(fstRoot->isDirectory(), true);
	BOOST_REQUIRE_EQUAL(fstRoot->recursiveCount(), 5);
	FstDirectory &fstRootDirectory = static_cast<FstDirectory &>(*fstRoot);
	BOOST_REQUIRE_EQUAL(fstRootDirectory.count(), 3);

	FstEntry &fstTestFile1 = fstRootDirectory.getEntry(0);
	BOOST_REQUIRE_EQUAL(fstTestFile1.getParent(), &fstRootDirectory);
	BOOST_REQUIRE_EQUAL(fstTestFile1.getName(), "testfile1");
	BOOST_REQUIRE_EQUAL(fstTestFile1.getFullPath(), "testfile1");
	BOOST_REQUIRE_EQUAL(fstTestFile1.isDirectory(), false);
	BOOST_REQUIRE_EQUAL(fstTestFile1.recursiveCount(), 1);
	FstFile &fstTestFile1File = static_cast<FstFile &>(fstTestFile1);
	BOOST_REQUIRE_EQUAL(fstTestFile1File.getPosition(), 0x11111111);
	BOOST_REQUIRE_EQUAL(fstTestFile1File.getSize(), 0x22222222);

	FstEntry &fstTestFolder = fstRootDirectory.getEntry(1);
	BOOST_REQUIRE_EQUAL(fstTestFolder.getParent(), &fstRootDirectory);
	BOOST_REQUIRE_EQUAL(fstTestFolder.getName(), "testfolder");
	BOOST_REQUIRE_EQUAL(fstTestFolder.getFullPath(), "testfolder");
	BOOST_REQUIRE_EQUAL(fstTestFolder.isDirectory(), true);
	BOOST_REQUIRE_EQUAL(fstTestFolder.recursiveCount(), 2);
	FstDirectory &fstTestFolderDirectory = static_cast<FstDirectory &>(fstTestFolder);
	BOOST_REQUIRE_EQUAL(fstTestFolderDirectory.count(), 1);

	FstEntry &fstTestFile2 = fstTestFolderDirectory.getEntry(0);
	BOOST_REQUIRE_EQUAL(fstTestFile2.getParent(), &fstTestFolderDirectory);
	BOOST_REQUIRE_EQUAL(fstTestFile2.getName(), "testfile2");
	BOOST_REQUIRE_EQUAL(fstTestFile2.getFullPath(), "testfolder/testfile2");
	BOOST_REQUIRE_EQUAL(fstTestFile2.isDirectory(), false);
	BOOST_REQUIRE_EQUAL(fstTestFile2.recursiveCount(), 1);
	FstFile &fstTestFile2File = static_cast<FstFile &>(fstTestFile2);
	BOOST_REQUIRE_EQUAL(fstTestFile2File.getPosition(), 0x33333333);
	BOOST_REQUIRE_EQUAL(fstTestFile2File.getSize(), 0x44444444);

	FstEntry &fstTestFile3 = fstRootDirectory.getEntry(2);
	BOOST_REQUIRE_EQUAL(fstTestFile3.getParent(), &fstRootDirectory);
	BOOST_REQUIRE_EQUAL(fstTestFile3.getName(), "testfile3");
	BOOST_REQUIRE_EQUAL(fstTestFile3.getFullPath(), "testfile3");
	BOOST_REQUIRE_EQUAL(fstTestFile3.isDirectory(), false);
	BOOST_REQUIRE_EQUAL(fstTestFile3.recursiveCount(), 1);
	FstFile &fstTestFile3File = static_cast<FstFile &>(fstTestFile3);
	BOOST_REQUIRE_EQUAL(fstTestFile3File.getPosition(), 0x55555555);
	BOOST_REQUIRE_EQUAL(fstTestFile3File.getSize(), 0x66666666);

	std::vector<std::reference_wrapper<FstEntry>> rootSubEntries = fstRootDirectory.recursiveGetEntries();
	BOOST_REQUIRE_EQUAL(rootSubEntries.size(), 5);
	BOOST_REQUIRE_EQUAL(&rootSubEntries[0].get(), &fstRootDirectory);
	BOOST_REQUIRE_EQUAL(&rootSubEntries[1].get(), &fstTestFile1File);
	BOOST_REQUIRE_EQUAL(&rootSubEntries[2].get(), &fstTestFolderDirectory);
	BOOST_REQUIRE_EQUAL(&rootSubEntries[3].get(), &fstTestFile2File);
	BOOST_REQUIRE_EQUAL(&rootSubEntries[4].get(), &fstTestFile3File);

	std::vector<std::reference_wrapper<FstEntry>> folderSubEntries = fstTestFolderDirectory.recursiveGetEntries();
	BOOST_REQUIRE_EQUAL(folderSubEntries.size(), 2);
	BOOST_REQUIRE_EQUAL(&folderSubEntries[0].get(), &fstTestFolderDirectory);
	BOOST_REQUIRE_EQUAL(&folderSubEntries[1].get(), &fstTestFile2File);

	std::vector<std::reference_wrapper<FstEntry>> fileSubEntries = fstTestFile2.recursiveGetEntries();
	BOOST_REQUIRE_EQUAL(fileSubEntries.size(), 1);
	BOOST_REQUIRE_EQUAL(&fileSubEntries[0].get(), &fstTestFile2File);
}

/**
 * Test with a fst.bin with a entry with an invalid type.
 */
BOOST_AUTO_TEST_CASE( test_read_fst_invalid_entry_type )
{
	const std::vector<std::uint8_t> fstBytes = {
		/* Root */
		0x01 /* Type flag */, 0x00, 0x00, 0x00 /* String table offset */,
		0x00, 0x00, 0x00, 0x00, /* Parent entry number */
		0x00, 0x00, 0x00, 0x02, /* Last entry number */
		/* testfile */
		0x12 /* Type flag (INVALID) */, 0x00, 0x00, 0x00 /* String table offset */,
		0x11, 0x11, 0x11, 0x11, /* ??? */
		0x22, 0x22, 0x22, 0x22, /* ??? */
		't', 'e', 's', 't', 'f', 'i', 'l', 'e', '\0' /* Note the lack of NUL terminator */
	};

	BOOST_REQUIRE_THROW(readFstFile(fstBytes), InvalidFstFileException);
}

/**
 * Test with a fst.bin with a entry whose name is not NUL terminated.
 */
BOOST_AUTO_TEST_CASE( test_read_fst_name_not_terminated )
{
	const std::vector<std::uint8_t> fstBytes = {
		/* Root */
		0x01 /* Type flag */, 0x00, 0x00, 0x00 /* String table offset */,
		0x00, 0x00, 0x00, 0x00, /* Parent entry number */
		0x00, 0x00, 0x00, 0x02, /* Last entry number */
		/* testfile */
		0x00 /* Type flag */, 0x00, 0x00, 0x00 /* String table offset */,
		0x11, 0x11, 0x11, 0x11, /* File offset */
		0x22, 0x22, 0x22, 0x22, /* File size */
		't', 'e', 's', 't', 'f', 'i', 'l', 'e' /* INVALID: Lack of NUL terminator */
	};

	BOOST_REQUIRE_THROW(readFstFile(fstBytes), InvalidFstFileException);
}

/**
 * Test with a fst.bin with a directory entry whose name points out of bounds.
 */
BOOST_AUTO_TEST_CASE( test_read_fst_name_out_of_bounds )
{
	const std::vector<std::uint8_t> fstBytes = {
		/* Root */
		0x01 /* Type flag */, 0x00, 0x00, 0x00 /* String table offset */,
		0x00, 0x00, 0x00, 0x00, /* Parent entry number */
		0x00, 0x00, 0x00, 0x02, /* Last entry number */
		/* testfile */
		0x00 /* Type flag */, 0x00, 0x00, 0x55 /* String table offset (INVALID) */,
		0x11, 0x11, 0x11, 0x11, /* File offset */
		0x22, 0x22, 0x22, 0x22, /* File size */
		't', 'e', 's', 't', 'f', 'i', 'l', 'e', '\0' /* Unused */
	};

	BOOST_REQUIRE_THROW(readFstFile(fstBytes), InvalidFstFileException);
}

/**
 * Test with a fst.bin with a directory entry whose parent entry number is not correct.
 */
BOOST_AUTO_TEST_CASE( test_read_fst_invalid_parent_entry_no )
{
	const std::vector<std::uint8_t> fstBytes = {
		/* Root */
		0x01 /* Type flag */, 0x00, 0x00, 0x00 /* String table offset */,
		0x00, 0x00, 0x00, 0x00, /* Parent entry number */
		0x00, 0x00, 0x00, 0x02, /* Last entry number */
		/* testfolder */
		0x01 /* Type flag */, 0x00, 0x00, 0x00 /* String table offset */,
		0x12, 0x34, 0x56, 0x78, /* Parent entry number (INVALID) */
		0x00, 0x00, 0x00, 0x02, /* Last entry number */
		't', 'e', 's', 't', 'f', 'o', 'l', 'd', 'e', 'r', '\0'
	};

	BOOST_REQUIRE_THROW(readFstFile(fstBytes), InvalidFstFileException);
}

/**
 * Test with a fst.bin with a directory entry whose last entry is overflown by sub-entries,
 * i.e. the last entry number is too low for the actual number of child sub-entries.
 */
BOOST_AUTO_TEST_CASE( test_read_fst_last_entry_no_overflow )
{
	const std::vector<std::uint8_t> fstBytes = {
		/* Root */
		0x01 /* Type flag */, 0x00, 0x00, 0x00 /* String table offset */,
		0x00, 0x00, 0x00, 0x00, /* Parent entry number */
		0x00, 0x00, 0x00, 0x04, /* Last entry number */
		/* testfolder1 */
		0x01 /* Type flag */, 0x00, 0x00, 0x00 /* String table offset */,
		0x00, 0x00, 0x00, 0x00, /* Parent entry number */
		0x00, 0x00, 0x00, 0x03, /* Last entry number (INVALID) */
		/* testfolder2 */
		0x01 /* Type flag */, 0x00, 0x00, 0x0C /* String table offset */,
		0x00, 0x00, 0x00, 0x01, /* Parent entry number */
		0x00, 0x00, 0x00, 0x04, /* Last entry number */
		/* testfile */
		0x00 /* Type flag */, 0x00, 0x00, 0x18 /* String table offset */,
		0x11, 0x11, 0x11, 0x11, /* File offset */
		0x22, 0x22, 0x22, 0x22, /* File size */
		't', 'e', 's', 't', 'f', 'o', 'l', 'd', 'e', 'r', '1', '\0',
		't', 'e', 's', 't', 'f', 'o', 'l', 'd', 'e', 'r', '2', '\0',
		't', 'e', 's', 't', 'f', 'i', 'l', 'e', '\0'
	};

	BOOST_REQUIRE_THROW(readFstFile(fstBytes), InvalidFstFileException);
}

/**
 * Test with a fst.bin with a directory entry whose last entry is too low,
 * i.e. the last entry number is smaller or equal than the current entry number
 */
BOOST_AUTO_TEST_CASE( test_read_fst_last_entry_no_underflow )
{
	const std::vector<std::uint8_t> fstBytes = {
		/* Root */
		0x01 /* Type flag */, 0x00, 0x00, 0x00 /* String table offset */,
		0x00, 0x00, 0x00, 0x00, /* Parent entry number */
		0x00, 0x00, 0x00, 0x02, /* Last entry number */
		/* testfolder1 */
		0x01 /* Type flag */, 0x00, 0x00, 0x00 /* String table offset */,
		0x00, 0x00, 0x00, 0x00, /* Parent entry number */
		0x00, 0x00, 0x00, 0x01, /* Last entry number (INVALID) */
		't', 'e', 's', 't', 'f', 'o', 'l', 'd', 'e', 'r', '1', '\0',
	};

	BOOST_REQUIRE_THROW(readFstFile(fstBytes), InvalidFstFileException);
}

/**
 * Write a minimalistic fst.bin with only a empty root directory.
 */
BOOST_AUTO_TEST_CASE( test_write_fst_valid_minimalistic )
{
	FstDirectory fstRootDirectory("");

	std::vector<std::uint8_t> fstBytes = writeFstFile(fstRootDirectory);

	const std::vector<std::uint8_t> expectedFstBytes = {
		0x01 /* Type flag */, 0x00, 0x00, 0x00 /* String table offset */,
		0x00, 0x00, 0x00, 0x00, /* Parent entry number */
		0x00, 0x00, 0x00, 0x01 /* Last entry number */ };
	BOOST_REQUIRE(fstBytes == expectedFstBytes);
}

/**
 * Write a fst.bin whose root entry is a file.
 */
BOOST_AUTO_TEST_CASE( test_write_fst_root_is_file )
{
	FstFile fstRootFile("", 0x11111111, 0x22222222);

	BOOST_REQUIRE_THROW(writeFstFile(fstRootFile), InvalidFstFileException);
}

/**
 * Write a fst.bin whose root entry has a parent entry.
 */
BOOST_AUTO_TEST_CASE( test_write_fst_root_has_parent )
{
	FstDirectory fstRootDirectory("");
	fstRootDirectory.add(std::move(std::unique_ptr<FstDirectory>(new FstDirectory("name"))));

	BOOST_REQUIRE_THROW(writeFstFile(fstRootDirectory.getEntry(0)), InvalidFstFileException);
}

/**
 * Write a fst.bin whose root entry has a name.
 */
BOOST_AUTO_TEST_CASE( test_write_fst_root_has_name )
{
	FstDirectory fstRootDirectory("some_random_name");

	BOOST_REQUIRE_THROW(writeFstFile(fstRootDirectory), InvalidFstFileException);
}

/**
 * Write a regular fst.bin with a few entries.
 * Root:
 *   testfile1
 *   testfolder:
 *     testfile2
 *   testfile3
 */
BOOST_AUTO_TEST_CASE( test_write_fst_valid_regular )
{
	FstDirectory fstRootDirectory("");
	std::unique_ptr<FstFile> testFile1(new FstFile("testfile1", 0x11111111, 0x22222222));
	std::unique_ptr<FstDirectory> testFolder(new FstDirectory("testfolder"));
	std::unique_ptr<FstFile> testFile2(new FstFile("testfile2", 0x33333333, 0x44444444));
	std::unique_ptr<FstFile> testFile3(new FstFile("testfile3", 0x55555555, 0x66666666));
	testFolder->add(std::move(testFile2));
	fstRootDirectory.add(std::move(testFile1));
	fstRootDirectory.add(std::move(testFolder));
	fstRootDirectory.add(std::move(testFile3));

	std::vector<std::uint8_t> fstBytes = writeFstFile(fstRootDirectory);

	const std::vector<std::uint8_t> expectedFstBytes = {
		/* Root */
		0x01 /* Type flag */, 0x00, 0x00, 0x00 /* String table offset */,
		0x00, 0x00, 0x00, 0x00, /* Parent entry number */
		0x00, 0x00, 0x00, 0x05, /* Last entry number */
		/* testfile1 */
		0x00 /* Type flag */, 0x00, 0x00, 0x00 /* String table offset */,
		0x11, 0x11, 0x11, 0x11, /* File offset */
		0x22, 0x22, 0x22, 0x22, /* File size */
		/* testfolder */
		0x01 /* Type flag */, 0x00, 0x00, 0x0A /* String table offset */,
		0x00, 0x00, 0x00, 0x00, /* Parent entry number */
		0x00, 0x00, 0x00, 0x04, /* Last entry number */
		/* testfile2 */
		0x00 /* Type flag */, 0x00, 0x00, 0x15 /* String table offset */,
		0x33, 0x33, 0x33, 0x33, /* Parent entry number */
		0x44, 0x44, 0x44, 0x44, /* Last entry number */
		/* testfile3 */
		0x00 /* Type flag */, 0x00, 0x00, 0x1F /* String table offset */,
		0x55, 0x55, 0x55, 0x55, /* Parent entry number */
		0x66, 0x66, 0x66, 0x66, /* Last entry number */
		't', 'e', 's', 't', 'f', 'i', 'l', 'e', '1', '\0',
		't', 'e', 's', 't', 'f', 'o', 'l', 'd', 'e', 'r', '\0',
		't', 'e', 's', 't', 'f', 'i', 'l', 'e', '2', '\0',
		't', 'e', 's', 't', 'f', 'i', 'l', 'e', '3', '\0'
	};
	BOOST_REQUIRE(fstBytes == expectedFstBytes);
}

/**
 * Write a fst.bin with so many names, the nametable is too big to fit.
 * This test is quite memory heavy, but should be no problem to modern systems.
 */
BOOST_AUTO_TEST_CASE( test_write_fst_nametable_overflow )
{
	// The maximum capacity of the nametable is 16777215 bytes
	// If we add 559241 files with a name of (29+1) bytes, we will surpass this limit
	FstDirectory fstRootDirectory("");
	for (size_t i = 0; i < 559241; i++)
	{
		std::unique_ptr<FstFile> file(new FstFile("this_string_has_29_characters", 0x11111111, 0x22222222));
		fstRootDirectory.add(std::move(file));
	}

	BOOST_REQUIRE_THROW(writeFstFile(fstRootDirectory), InvalidFstFileException);
}
