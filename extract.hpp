/*
OpenGCReEx - Open source GameCube ISO extractor / compiler.
Copyright (C) 2015-2016 Joan Bruguera Micó <joanbrugueram@gmail.com>

This file is part of OpenGCReEx.

OpenGCReEx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenGCReEx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenGCReEx.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef OPENGCREEX_EXTRACT_HPP
#define OPENGCREEX_EXTRACT_HPP

#include "filesystem.hpp"

/**
 * Extract the contents from a GameCube ISO to a Gamecube filesystem.
 * Params:
 *     isoPath: Path where the Gamecube ISO/GCM file resides.
 *     fsPath: Path where the Gamecube filesystem should be written.
 */
void extractGcIso(const filesystem::path &isoPath, const filesystem::path &fsPath);

#endif // OPENGCREEX_EXTRACT_HPP
