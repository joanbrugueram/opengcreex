/*
OpenGCReEx - Open source GameCube ISO extractor / compiler.
Copyright (C) 2015-2016 Joan Bruguera Micó <joanbrugueram@gmail.com>

This file is part of OpenGCReEx.

OpenGCReEx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenGCReEx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenGCReEx.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef OPENGCREEX_FST_HPP
#define OPENGCREEX_FST_HPP

#include <cstdint>
#include <vector>
#include <memory>
#include <string>
#include <functional>
#include <stdexcept>
#include "filesystem.hpp"

// Forward declaration for use in FstEntry
class FstFile;
class FstDirectory;

/**
 * Represents a generic entry (file or directory) inside a Gamecube FST filesystem descriptor.
 */
class FstEntry
{
	/**
	 * Parent entry to this entry, or nullptr if this is the root entry.
	 */
	FstEntry *parent;

	/**
	 * Name of this entry, or an empty string if this is the root entry.
	 */
	std::string name;

private:
	/**
	 * Creates a new FstEntry with the given name and no assigned parent.
	 * Params:
	 *     pName: The name to assign to this entry.
	 */
	FstEntry(const std::string &pName);

	/**
	 * Set the parent entry to this entry.
	 * Params:
	 *     pParent: Entry to set as parent to this entry.
	 */
	void setParent(FstEntry *pParent);

	// Only allow those two classes to call the ctor. (inherit) or setParent()
	friend class FstDirectory;
	friend class FstFile;

public:
	// Need to declare virtual destructor to ensure subclasses are destroyed correctly
	virtual ~FstEntry();

	/**
	 * Get the parent entry to this entry.
	 * Returns: Parent entry to this entry, or nullptr if this is the root entry.
	 */
	FstEntry *getParent();

	/**
	 * Get the parent entry to this entry.
	 * Returns: Parent entry to this entry, or nullptr if this is the root entry.
	 */
	const FstEntry *getParent() const;

	/**
	 * Get the name of this entry.
	 * Name of this entry, or an empty string if this is the root entry.
	 */
	const std::string &getName() const;

	/**
	 * Get the full path to this entry (names from the parent up to this entry, separated by directory separator).
	 * Returns: The full path to this entry (names from the parent up to this entry, separated by directory separator).
	 */
	filesystem::path getFullPath() const;

	/**
	 * Get a vector containing a reference to all sub-entries of this entry, including itself, recursively.
	 * The order of the returned entries is the same as in the FST, e.g. directories come before their files.
	 * Returns: A vector containing a reference to all sub-entries of this entry, including itself, recursively.
	 */
	std::vector<std::reference_wrapper<FstEntry>> recursiveGetEntries();

	/**
	 * Get a vector containing a reference to all sub-entries of this entry, including itself, recursively.
	 * The order of the returned entries is the same as in the FST, e.g. directories come before their files.
	 * Returns: A vector containing a reference to all sub-entries of this entry, including itself, recursively.
	 */
	std::vector<std::reference_wrapper<const FstEntry>> recursiveGetEntries() const;

	/**
	 * Count the number of entries and subentries in this entry (including itself).
	 * Returns: The number of entries contained in this entry.
	 */
	virtual std::size_t recursiveCount() const = 0;

	/**
	 * Checks if this entry corresponds to a directory entry.
	 * Returns: True if the entry is a directory, false otherwise.
	 */
	virtual bool isDirectory() const = 0;

protected:
	/**
	 * Push all entries and sub-entries of this entry, including itself, to the container given as a parameter.
	 * Params:
	 *     cont: A vector containing a reference to all sub-entries of this entry, including itself, recursively.
	 */
	virtual void recursivePushEntries(std::vector<std::reference_wrapper<FstEntry>> &cont) = 0;

	/**
	 * Push all entries and sub-entries of this entry, including itself, to the container given as a parameter.
	 * Params:
	 *     cont: A vector containing a reference to all sub-entries of this entry, including itself, recursively.
	 */
	virtual void recursivePushEntries(std::vector<std::reference_wrapper<const FstEntry>> &cont) const = 0;
};

/**
 * Represents a directory entry inside a Gamecube FST filesystem descriptor.
 */
class FstDirectory : public FstEntry
{
	/**
	 * List of subentries to this directory entry in the FST.
	 */
	std::vector<std::unique_ptr<FstEntry>> subEntries;

public:
	/**
	 * Create a new empty directory entry.
	 * Params:
	 *     pName: Name of the directory.
	 */
	FstDirectory(const std::string &pName);

	/**
	 * Add a new entry inside this directory.
	 * Params:
	 *     entry: The entry to move into this directory.
	 */
	void add(std::unique_ptr<FstEntry> &&entry);

	/**
	 * Get the number of entries inside this directory.
	 * Returns: The number of entries inside this directory.
	 */
	std::size_t count() const;

	/**
	 * Get the entry inside this directory corresponding to the i'th position.
	 * Returns: The entry corresponding to the i'th position in the directory.
	 */
	FstEntry &getEntry(std::size_t i);

	/**
	 * Get the entry inside this directory corresponding to the i'th position.
	 * Returns: The entry corresponding to the i'th position in the directory.
	 */
	const FstEntry &getEntry(std::size_t i) const;

	std::size_t recursiveCount() const override;

	bool isDirectory() const override;

protected:
	void recursivePushEntries(std::vector<std::reference_wrapper<FstEntry>> &cont) override;

	void recursivePushEntries(std::vector<std::reference_wrapper<const FstEntry>> &cont) const override;
};

/**
 * Represents a file entry inside a Gamecube FST filesystem descriptor.
 */
class FstFile : public FstEntry
{
	/**
	 * Position of the file relative to the beginning of the ISO.
	 */
	std::uint32_t position;

	/**
	 * Size of the file in the ISO.
	 */
	std::uint32_t size;

public:
	/**
	 * Create a new file entry-
	 * Params:
	 *     pName: Name of the file.
	 *     pPosition: Position of the file relative to the beginning of the ISO.
	 *     pSize: Size of the file.
	 */
	FstFile(const std::string &pName, std::uint32_t pPosition, std::uint32_t pSize);

	/**
	 * Get the position of the file in the ISO.
	 * Returns: Position of the file relative to the beginning of the ISO.
	 */
	std::uint32_t getPosition() const;

	/**
	 * Set the position of the file in the ISO.
	 * Params:
	 *     pPosition: Position of the file relative to the beginning of the ISO.
	 */
	void setPosition(std::uint32_t pPosition);

	/**
	 * Get the size of the file in the ISO.
	 * Returns: Size of the file in the ISO.
	 */
	std::uint32_t getSize() const;

	/**
	 * Set the size of the file in the ISO.
	 * Params:
	 *     pSize: Size of the file in the ISO.
	 */
	void setSize(std::uint32_t pSize);

	std::size_t recursiveCount() const override;

	bool isDirectory() const override;

protected:
	void recursivePushEntries(std::vector<std::reference_wrapper<FstEntry>> &cont) override;

	void recursivePushEntries(std::vector<std::reference_wrapper<const FstEntry>> &cont) const override;
};

/**
 * Exception class thrown when attempting to read or write an invalid fst.bin structure.
 */
class InvalidFstFileException : public std::runtime_error
{
public:
	explicit InvalidFstFileException(const std::string &what_arg);
};

/**
 * Read the contents of a fst.bin file to a FST filesystem.
 * Params:
 *     fstBinFile: Byte stream corresponding to a fst.bin file.
 * Returns: A pointer to the root directory inside the fst.bin file.
 */
std::unique_ptr<FstEntry> readFstFile(const std::vector<std::uint8_t> &fstBinFile);

/**
 * Write the contents of a FST filesystem to a fst.bin file.
 * Params:
 *     rootDirectory: A reference to the root directory inside the fst.bin file.
 * Returns: Byte stream corresponding to a fst.bin file.
 */
std::vector<std::uint8_t> writeFstFile(const FstEntry &rootDirectory);

#endif // OPENGCREEX_FST_HPP
