/*
OpenGCReEx - Open source GameCube ISO extractor / compiler.
Copyright (C) 2015-2016 Joan Bruguera Micó <joanbrugueram@gmail.com>

This file is part of OpenGCReEx.

OpenGCReEx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenGCReEx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenGCReEx.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>
#include "extract.hpp"
#include "compile.hpp"

const char *applicationName = "OpenGCReEx";
const char *applicationVersion = "0.1";

int main(int argc, char *argv[])
{
	// Turn off synchronization between standard C++ and standard C streams,
	// since we are not using C streams, and this has HUGE performance benefits
	std::ios_base::sync_with_stdio(false);

	// Show title screen
	std::cout << applicationName << " " << applicationVersion << std::endl;
	std::cout << std::endl;

	// Parse command line args
	std::vector<std::string> args(argv, argv + argc);
	if ((args.size() == 3 || args.size() == 4) && args[1] == "-x")
	{
		std::string inputIso = args[2];
		std::string outputFolder = (args.size() == 4) ? args[3] : ".";
		try
		{
			extractGcIso(inputIso, outputFolder);
		}
		catch (std::exception &ex)
		{
			std::cout << "Extraction error" << std::endl;
			std::cout << ex.what() << std::endl;
			return EXIT_FAILURE;
		}
	}
	else if ((args.size() == 3 || args.size() == 4) && (args[1] == "-c" || args[1] == "-c2"))
	{
		bool isDisc2 = (args[1] == "-c2");
		std::string inputFolder = args[2];
		std::string outputIso = (args.size() == 4) ? args[3] : (!isDisc2 ? "out.iso" : "out2.iso");

		try
		{
			compileGcIso(inputFolder, outputIso, isDisc2);
		}
		catch (std::exception &ex)
		{
			std::cout << "Compilation error" << std::endl;
			std::cout << ex.what() << std::endl;
			return EXIT_FAILURE;
		}
	}
	else
	{
		std::cout << "Usage: " << std::endl;
		std::cout << " " << args[0] << " [-x|-c] [game.iso|folder]";
		std::cout << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
