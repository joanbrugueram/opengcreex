/*
OpenGCReEx - Open source GameCube ISO extractor / compiler.
Copyright (C) 2015-2016 Joan Bruguera Micó <joanbrugueram@gmail.com>

This file is part of OpenGCReEx.

OpenGCReEx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenGCReEx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenGCReEx.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef OPENGCREEX_STREAMUTILS_HPP
#define OPENGCREEX_STREAMUTILS_HPP

#include <iostream>
#include <cstdint>
#include <vector>
#include "filesystem.hpp"
#include "strformat.hpp"

/**
* Define those types to store file offsets and file sizes respectively.
* This is to avoid cross-platform issues (e.g. compiling on 32-bit systems),
* and overflow issues (e.g. to avoid overflow when adding 32-bit types over size_t).
*/
typedef std::uint64_t fileoffset_t;
static_assert(sizeof(fileoffset_t) >= sizeof(std::size_t),
	"fileoffset_t must be greater than size_t");

typedef std::uint64_t filesize_t;
static_assert(sizeof(filesize_t) >= sizeof(std::size_t),
	"filesize_t must be greater than size_t");

/**
 * Opens an input file, as binary, with exceptions enabled.
 * Additionally, in case of error, it will throw an exception with a more readable message.
 * Params:
 *     path: Path of the file to open for reading.
 * Returns: Input file stream associated with the file on the specified path.
 */
std::ifstream openInputBinaryFileWithExceptions(const filesystem::path &path);

/**
 * Set the input stream read position to the given position.
 * Params:
 *     stream: Stream where the read position should be set.
 *     position: Position to set from the beginning of the stream.
 */
void setInputStreamPosition(std::istream &stream, fileoffset_t position);

/**
 * Get the size of a stream.
 * Params:
 *     stream: Stream from which to get the size.
 * Returns: The size of the stream.
 */
filesize_t getStreamSize(std::istream &stream);

/**
 * Read a chunk of data from an input stream.
 * Params:
 *     stream: Stream from which the data should be read.
 *     buffer: Buffer where the data should be stored.
 *     size: Number of bytes to read into the buffer.
 */
void readFromInputStream(std::istream &stream, std::uint8_t *buffer, std::size_t size);

/**
 * Opens an output file, as binary, with exceptions enabled.
 * Additionally, in case of error, it will throw an exception with a more readable message.
 * Params:
 *     path: Path of the file to open for writing.
 * Returns: Output file stream associated with the file on the specified path.
 */
std::ofstream openOutputBinaryFileWithExceptions(const filesystem::path &path);

/**
 * Read a chunk of data to an output stream.
 * Params:
 *     stream: Stream to which the data should be written.
 *     buffer: Buffer where the data should be read from.
 *     size: Number of bytes to write to the output stream.
 */
void writeToOutputStream(std::ostream &stream, const std::uint8_t *buffer, std::size_t size);

/**
 * Pad a stream with zeroes until its position is a multiple of the given alignment boundary.
 * https://en.wikipedia.org/wiki/Data_structure_alignment#Computing_padding
 * Params:
 *     stream: Stream to pad.
 *     alignment: The alignment boundary. Must be a power of two.
 */
void alignStream(std::ostream &stream, filesize_t alignment);

/**
 * Read the contents of a file to memory.
 * Params:
 *     stream: Stream from which the chunk should be extracted.
 *     position: Position inside the stream where the chunk begins.
 *     size: Size of the chunk inside the stream.
 *     outPath: Path where the extracted chunk should be written.
 */
std::vector<std::uint8_t> readFileToMemory(const filesystem::path &path);

/**
 * Extract the contents of a chunk of the specified stream to a file.
 * Params:
 *     stream: Stream from which the chunk should be extracted.
 *     position: Position inside the stream where the chunk begins.
 *     size: Size of the chunk inside the stream.
 *     outPath: Path where the extracted chunk should be written.
 */
void extractToFile(std::istream &stream, fileoffset_t position, filesize_t size,
	const filesystem::path &outPath);

/**
 * Copy the specified number of bytes from the input stream to the output stream.
 * Params:
 *     inStream: Stream from which to read the data.
 *     outStream: Stream to which to read the data.
 *     size: Number of bytes to copy from input to output.
 */
void copyStream(std::istream &inStream, std::ostream &outStream, filesize_t size);

/**
 * Extract a big-endian integer of the specified type from a buffer.
 * Params:
 *     buffer: Buffer containing the big-endian integer to extract.
 *     pos: Position in the buffer of the first byte of the big-endian integer.
 * Returns: The extracted integer in native format.
 */
template<typename T>
static inline T extractBigEndian(const std::uint8_t *buffer, std::size_t pos)
{
	T value = 0;
	for (std::size_t i = 0; i < sizeof(T); i++)
		value |= static_cast<T>(buffer[pos + i]) << ((sizeof(T) - i - 1) * 8);
	return value;
}

/**
 * Write a big-endian integer of the specified type to a buffer.
 * Params:
 *     buffer: Buffer where the big-endian integer should be written.
 *     pos: Position in the buffer of the first byte of the big-endian integer.
 *     value: Integer value to write.
 */
template<typename T>
static void writeBigEndian(std::uint8_t *buffer, std::size_t pos, T value)
{
	for (std::size_t i = 0; i < sizeof(T); i++)
		buffer[pos + i] = static_cast<std::uint8_t>(value >> ((sizeof(T) - i - 1) * 8));
}

#endif // OPENGCREEX_STREAMUTILS_HPP
